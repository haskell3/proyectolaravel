<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Models\Publicacio;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

$userController = \App\Http\Controllers\UserController::class;

Route::get('/', [$userController, 'index'])->name('index');

Route::get('/profile/adds', [$userController, 'showAdds'])->name('showAdds');

Route::get('/profile/saved', [$userController, 'showSaved'])->name('showSaved');

Route::get('/profile/newAdd', [$userController, 'showNewAddForm'])->name('showNewAddForm');

Route::get('/selectedAdd/{idPublicacio}', [$userController, 'selectedAdd'])->name('selectedAdd');

Route::get('/add/{idPublicacio}/newComment/{text}', [$userController, 'newCommentNotRequest'])->name('newCommentNotRequest');

Route::post('/storeNewAdd', [$userController, 'storeNewAdd'])->name('storeNewAdd');

Route::post('/guardar-favorito', [$userController, 'guardar'])->name('guardarFavorito');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
