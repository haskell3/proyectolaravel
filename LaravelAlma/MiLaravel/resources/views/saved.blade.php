<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ofertas Guardadas</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100">

<div class="max-w-4xl mx-auto px-4 py-8">
    <h1 class="text-3xl font-bold mb-6">Ofertas Guardadas por {{ $user->name }}</h1>

    <ul class="list-disc pl-4">
        @foreach($ofertasGuardadas as $oferta)
            <li class="text-lg text-gray-800 mb-2">{{ $oferta->marca }} - {{ $oferta->modelo }}</li>
            <!-- Ajusta las propiedades según la estructura de tu modelo Publicacio -->
        @endforeach
    </ul>
</div>

</body>
</html>
