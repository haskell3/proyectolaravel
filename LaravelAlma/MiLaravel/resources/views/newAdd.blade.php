<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear nueva publicacion</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100">

<div class="max-w-md mx-auto mt-10 px-4 py-8 bg-white shadow-md rounded-lg">
    <h1 class="text-2xl font-bold mb-6">Formulario para crear una nueva oferta de coche</h1>

    <form action="{{ route('storeNewAdd') }}" method="POST">
        @csrf
        <div class="mb-4">
            <label for="titulo" class="block text-sm font-medium text-gray-700">Título:</label>
            <input type="text" id="titulo" name="titulo" class="mt-1 p-2 block w-full border rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="descripcion" class="block text-sm font-medium text-gray-700">Descripción:</label>
            <input type="text" id="descripcion" name="descripcion" class="mt-1 p-2 block w-full border rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="marca" class="block text-sm font-medium text-gray-700">Marca:</label>
            <input type="text" id="marca" name="marca" class="mt-1 p-2 block w-full border rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="modelo" class="block text-sm font-medium text-gray-700">Modelo:</label>
            <input type="text" id="modelo" name="modelo" class="mt-1 p-2 block w-full border rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="ano" class="block text-sm font-medium text-gray-700">Año:</label>
            <input type="number" id="ano" name="ano" class="mt-1 p-2 block w-full border rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-4">
            <label for="precio" class="block text-sm font-medium text-gray-700">Precio:</label>
            <input type="number" id="precio" name="precio" class="mt-1 p-2 block w-full border rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500">
        </div>
        <div class="mb-6">
            <label for="kilometros" class="block text-sm font-medium text-gray-700">Kilometros:</label>
            <input type="number" id="kilometros" name="kilometros" class="mt-1 p-2 block w-full border rounded-md shadow-sm focus:ring-blue-500 focus:border-blue-500">
        </div>
        <button type="submit" class="w-full px-4 py-2 bg-blue-500 text-white rounded-md shadow-md hover:bg-blue-600">Crear Oferta</button>
    </form>
</div>

</body>
</html>
