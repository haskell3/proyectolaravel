<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Publicaciones</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100">

<div class="max-w-4xl mx-auto px-4 py-8">
    @foreach($publicacions as $publicacio)
        <a href="{{ route('selectedAdd', $publicacio->id) }}" class="block mb-4 hover:bg-gray-50 rounded-lg shadow-md px-6 py-4 flex justify-between items-center">
            <p class="text-lg font-semibold text-gray-800">{{ $publicacio->titol }}</p>
            <form method="POST" action="{{ route('guardarFavorito') }}" class="inline-block">
                @csrf
                <input type="hidden" name="publicacio_id" value="{{ $publicacio->id }}">
                <button type="submit" class="bg-blue-500 hover:bg-blue-600 text-white px-4 py-2 rounded-md">Guardar como favorito</button>
            </form>
        </a>
    @endforeach
</div>

</body>
</html>
