<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detalles de la Oferta</title>
    <link href="https://cdn.jsdelivr.net/npm/tailwindcss@2.2.19/dist/tailwind.min.css" rel="stylesheet">
</head>
<body class="bg-gray-100">

<div class="max-w-4xl mx-auto px-4 py-8">
    <h1 class="text-3xl font-bold mb-6">Detalles de la Oferta</h1>

    <div class="bg-white shadow-md rounded-lg p-8">
        <h2 class="text-xl font-bold mb-4">{{ $publicacio->titol }}</h2>
        <p class="text-gray-600 mb-6">{{ $publicacio->descripcio }}</p>

        <!-- Información del coche -->
        <div class="grid grid-cols-2 gap-4">
            <div>
                <p class="text-lg font-semibold mb-2">Cotxe</p>
                <p><span class="font-semibold">Marca:</span> {{ $cotxe->marca }}</p>
                <p><span class="font-semibold">Model:</span> {{ $cotxe->model }}</p>
                <p><span class="font-semibold">Any:</span> {{ $cotxe->any }}</p>
            </div>
            <div>
                <p><span class="font-semibold">Preu:</span> {{ $cotxe->preu }}</p>
                <p><span class="font-semibold">Kilometres:</span> {{ $cotxe->kilometres }}</p>
            </div>
        </div>

        <!-- Comentarios -->
        <div class="mt-8">
            <h2 class="text-lg font-semibold mb-4">Comentaris</h2>
            <ul>
                @foreach($publicacio->comentaris as $comentari)
                    <li class="border-b border-gray-200 py-4">
                        <p class="text-lg font-semibold mb-2">{{ $comentari->usuari ? $comentari->usuari->name : 'Usuario desconocido' }}</p>
                        <p class="text-gray-600">{{ $comentari->text }}</p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

</div>

</body>
</html>
