<?php

namespace App\Http\Controllers;

use App\Models\Comentari;
use App\Models\Cotxe;
use App\Models\Publicacio;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function index()
    {
        $publicacions = Publicacio::orderBy('created_at', 'desc')->get();
        return view('index', ['publicacions' => $publicacions]);
    }

    public function showAdds()
    {
        // Obtenemos al usuario autenticado
        $user = Auth::user();

        // Verificamos si el usuario está autenticado
        if ($user) {
            // Si el usuario está autenticado, obtenemos sus publicaciones
            $publicaciones = $user->publicacio;
            return view('showAdds', compact('publicaciones', 'user'));
        } else {
            // Si el usuario no está autenticado, redirigimos al inicio de sesión
            return redirect()->route('auth.login');
        }
    }

    public function showSaved(){
        // Obtenemos al usuario autenticado
        $user = Auth::user();

        // Verificamos si el usuario está autenticado
        if ($user) {
            // Si el usuario está autenticado, obtenemos las ofertas guardadas del usuario
            $ofertasGuardadas = $user->publicacions_favorites;
            return view('showSaved', compact('ofertasGuardadas', 'user'));
        } else {
            // Si el usuario no está autenticado, redirigimos al inicio de sesión
            return redirect()->route('auth.login');
        }
    }

    public function showNewAddForm(){
        return view('newAdd');
    }


    public function storeNewAdd(Request $request){
        $user = Auth::user();
        // Validar los datos del formulario
        $request->validate([
            'titulo' => 'required|string',
            'descripcion' => 'required|string',
            'marca' => 'required|string',
            'modelo' => 'required|string',
            'ano' => 'required|string',
            'precio' => 'required|string',
            'kilometros' => 'required|string',
        ]);

        // Crear una nueva instancia de Publicacio con los datos del formulario
        $cotxe = new Cotxe();
        $cotxe->marca = $request->marca;
        $cotxe->model = $request->modelo;
        $cotxe->any = $request->ano;
        $cotxe->preu = $request->precio;
        $cotxe->kilometres = $request->kilometros;
        // Asigna otros campos del formulario según sea necesario

        // Guardar la nueva oferta en la base de datos
        $cotxe->save();

        $publicacio = new Publicacio();
        $publicacio->titol = $request->titulo;
        $publicacio->descripcio = $request->descripcion;
        $publicacio->cotxe_id = $cotxe->id;
        $publicacio->user_id = $user->id;

        $publicacio->save();
        return redirect()->route('index')->with('success', 'Publicacion creada correctamente.');

        // Redireccionar a una página de confirmación o mostrar un mensaje de éxito

    }


    public function newComment(Request $request){
        // Validar los datos del formulario
        $request->validate([
            'contenido' => 'required|string|max:255',
            // Puedes agregar más validaciones según tus necesidades
        ]);

        // Obtenemos al usuario autenticado
        $user = Auth::user();

        // Creamos una nueva instancia de Comentari con los datos del formulario
        $comentario = new Comentari();
        $comentario->text = $request->contenido; // 'contenido' es el nombre del campo en el formulario
        $comentario->usuari = $user; // Asignamos el ID del usuario autenticado al comentario
        $comentario->publicacio = Publicacio::find($request->publicacio_id); // Asignamos el ID de la publicación al comentario

        // Guardamos el comentario en la base de datos
        $comentario->save();

        // Redirigimos a alguna página de confirmación o de visualización del comentario creado
        return redirect()->back()->with('success', 'Comentario creado correctamente.');
    }

    public function newCommentNotRequest($idPublicacio, $text){

        $user = Auth::user();

        if($user){
            $comentario = new Comentari();
            $comentario->text = $text;
            $comentario->user_id = $user->id;
            $comentario->publicacio_id = $idPublicacio;


            $comentario->save();

            return redirect()->back()->with('success', 'Comentario creado correctamente.');
        }else{
            return redirect()->route('auth.login');
        }
    }

    public function guardar(Request $request)
    {
        //TODO: contemplar si ya lo tiene

        // Obtener el ID de la publicación desde la solicitud
        $publicacioId = $request->input('publicacio_id');

        // Obtener el usuario actual (asumiendo que estás autenticado)
        $user = Auth::user();

        // Asociar la publicación como favorita del usuario
        $user->publicacions_favorites()->attach($publicacioId);

        // Redireccionar o devolver una respuesta JSON según sea necesario
        return redirect()->back()->with('success', 'Publicación guardada como favorita');
    }

    public function selectedAdd($idPublicacio){
        $publicacio = Publicacio::find($idPublicacio);
        $cotxe = Cotxe::find($publicacio->cotxe_id);
        $comentaris = $publicacio->comentaris;
        return view('selectedAdd', ['publicacio' => $publicacio, 'cotxe' => $cotxe, 'comentaris' => $comentaris]);
    }
}
