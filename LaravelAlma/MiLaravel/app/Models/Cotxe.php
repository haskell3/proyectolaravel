<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cotxe extends Model
{
    use HasFactory;
    protected $fillable=['marca', 'model', 'any', 'preu', 'kilometres', 'imatge'];
    public function publicacio() {
        return $this->belongsTo(Publicacio::class);
    }
}
