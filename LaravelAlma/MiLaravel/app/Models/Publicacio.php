<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publicacio extends Model
{
    use HasFactory;

    protected $fillable = ['titol', 'descripcio'];

    public function usuari(){
        return $this->belongsTo(User::class);
    }

    public  function comentaris(){
        return $this->hasMany(Comentari::class);
    }

    public function cotxe(){
        return $this->hasOne(Cotxe::class);
    }

    public function beFavorite(){
        //Many to many with user
        return $this->belongsToMany(User::class, 'publicacions_users', 'publicacio_id', 'user_id');
    }
}
