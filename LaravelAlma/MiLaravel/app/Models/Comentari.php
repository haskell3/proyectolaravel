<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentari extends Model
{
    use HasFactory;
    protected $fillable = ['text'];

    public function usuari(){
        return $this->belongsTo(User::class);
    }

    public function publicacio(){
        return $this->belongsTo(Publicacio::class);
    }
}
