<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cotxe>
 */
class CotxeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'marca'=> Str::random(10),
            'model'=> Str::random(10),
            'any'=> rand(1886, 2024),
            'preu'=> rand(1, 10000),
            'kilometres'=> rand(1, 10000),
        ];
    }
}
