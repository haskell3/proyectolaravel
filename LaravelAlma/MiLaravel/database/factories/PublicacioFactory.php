<?php

namespace Database\Factories;

use App\Models\Cotxe;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Publicacio>
 */
class PublicacioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $userIds = User::pluck('id')->toArray();
        $cotxeIds = Cotxe::pluck('id')->toArray();

        return [
            'titol' => Str::random(10),
            'descripcio' => Str::random(10),
            'user_id' => $this->faker->randomElement($userIds),
            'cotxe_id' => $this->faker->randomElement($cotxeIds),
            // Otros campos de la publicación si los tienes
        ];
    }
}
