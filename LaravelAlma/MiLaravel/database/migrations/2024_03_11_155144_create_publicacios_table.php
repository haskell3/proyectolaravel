<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('publicacios', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('titol');
            $table->string('descripcio');
            $table->foreignId('user_id')->constrained('users', 'id');
            $table->foreignId('cotxe_id')->constrained('cotxes', 'id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('publicacios');
    }
};
